### Oscillatory behaviour of the RBF-FD approximation accuracy under increasing stencil size

This repository has all the code used to generate the above [ICCS 2023](https://www.iccs-meeting.org/iccs2023) conference paper.

#### Abstract

When solving partial differential equations on scattered nodes using the radial basis function generated finite difference (RBF-FD) method, one of the parameters that must be chosen is the stencil size. Focusing on Polyharmonic Spline RBFs with monomial augmentation, we observe that the stencil size affects the approximation accuracy in a particularly interesting way - the solution error dependence on stencil size has several local minima. We find that we can connect this behaviour with the spatial dependence of the signed approximation error. Based on this observation we are then able to introduce a numerical quantity that indicates whether a given stencil size is close to one of those local minima.

#### Authors
- Andrej Kolar-Požun
- Mitja Jančič
- Miha Rot
- Gregor Kosec

#### Repository structure overview
- **Code** C++ and Python codes used to generate the data and the figures.
- **FiguresPaper** All the figures used in the paper.
- **Paper** All the LaTeX files used to generate the paper.



For any questions regarding the paper we are available at andrej.pozun@ijs.si