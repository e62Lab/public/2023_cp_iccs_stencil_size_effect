#!/bin/bash

if [ ! -d "./Data" ]
then
echo Making the Data directory...
mkdir Data
fi

echo Running all the scripts...
for filename in PoissonSweep.cpp LaplaceOpSweep.cpp PoissonSweepBoundaryFixed.cpp PoissonSweepOtherh.cpp
do
echo Compiling $filename...
filenameBase=$(basename $filename .cpp)
g++ -o $filenameBase "${filenameBase}.cpp" -I ../medusa/include -I /usr/include/hdf5/serial -L ../medusa/bin/ -L /usr/lib/x86_64-linux-gnu/hdf5/serial/ -l medusa_standalone -l hdf5 -Wall
echo Running it...
./$filenameBase
rm $filenameBase
done

echo Running the Python script...
python3 generatePlots.py