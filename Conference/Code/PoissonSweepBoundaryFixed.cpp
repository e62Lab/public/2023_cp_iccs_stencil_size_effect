#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>

using namespace mm;
using namespace Eigen;

//SCRIPT TO PREPARE POISSON EQ. RESULTS WITH BOUNDARY LAYER FIXED

void solve(int which) { //which = 1 - boundary fixed. Which = 2 - interior fixed
    int m = 3;
    double dx = 0.01;

    BallShape<Vec2d> ball({0.5,0.5}, 0.5);
    
    DomainDiscretization<Vec2d> domain = ball.discretizeBoundaryWithStep(dx);
    GeneralFill<Vec2d> fill_engine;
    int seed = 1;
    fill_engine.seed(seed);
    domain.fill(fill_engine, dx);

    int N = domain.size();
    VectorXd rhs(N); rhs.setZero();

    for (int i : domain.all()){
        double x = domain.pos(i,0);
        double y = domain.pos(i,1);
        if ((x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) <= 0.4*0.4) domain.type(i) = 30; //inside layer
    }
    auto edge = (domain.types() != 30);
    auto middle = (domain.types() == 30);

    FindClosest findSupportFixed = FindClosest(28);
    if (which==1) findSupportFixed.forNodes(edge);
    else if (which == 2) findSupportFixed.forNodes(middle);
    domain.findSupport(findSupportFixed);


    std::ostringstream strs;
    if (which==1) strs << "Data/PoissonResultsBndFixed.h5";
    else if (which==2) strs << "Data/PoissonResultsIntFixed.h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);
    hdf_out.writeDouble2DArray("positions",domain.positions());

    int startN = 10;
    int toCheck = 60;

    for (int n=startN;n<startN+toCheck;++n){
        std::cout << n << std::endl;
        FindClosest findSupportVar = FindClosest(n);
        if (which==1) findSupportVar.forNodes(middle);
        else if (which==2) findSupportVar.forNodes(edge);
        domain.findSupport(findSupportVar);
        Monomials<Vec2d> mon(m);
        RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({}, mon);
        auto storage = domain.computeShapes(approx);
        SparseMatrix<double, Eigen::RowMajor> M(N, N);
        M.reserve(storage.supportSizes());
        rhs.setZero();
        auto op = storage.implicitOperators(M, rhs);

        for (int i : domain.interior()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.lap(i) = -2*PI*PI*(std::sin(PI*x)*std::sin(PI*y)); //u=sin(pix) sin(pi y)
        }
        for (int i : domain.boundary()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.value(i) = std::sin(PI*x)*std::sin(PI*y);
        }
        
        Eigen::SparseLU<decltype(M), COLAMDOrdering<int>> solver;
        solver.compute(M);
        ScalarFieldd u = solver.solve(rhs);
        strs.str("");
        strs << "SolutionN" << n;
        hdf_out.writeDoubleArray(strs.str(),u);

        VectorXd radius(N); radius.setZero();
        if (n==startN+toCheck-1){
            for (int i : domain.all()){
                auto supp = domain.supportNodes(i);
                radius(i) = (supp[0]-supp.back()).norm();
            }
            strs.str("");
            strs << "Radiuses";
            hdf_out.writeDoubleArray(strs.str(),radius);
        }
    }
    hdf_out.close();
}

int main(){
    solve(1);
    solve(2);
    return 0;
}
