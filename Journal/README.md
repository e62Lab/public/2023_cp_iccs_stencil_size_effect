### Some observations regarding the RBF-FD approximation accuracy dependence on stencil size

This repository has all the code used to generate the [Journal of Computational Science](https://www.sciencedirect.com/journal/journal-of-computational-science) ICCS 2023 special issue paper.

#### Abstract

When solving partial differential equations on scattered nodes using the Radial Basis
Function-generated Finite Difference (RBF-FD) method, one of the parameters that
must be chosen is the stencil size. Focusing on Polyharmonic Spline RBFs with
monomial augmentation, we observe that it affects the approximation accuracy in a
particularly interesting way - the solution error oscillates under increasing stencil size.
We find that we can connect this behaviour with the spatial dependence of the signed
approximation error. Based on this observation we are able to introduce a numerical
quantity that could indicate whether a given stencil size is locally optimal. This work is
an extension of our ICCS 2023 conference paper [1].

#### Authors
- Andrej Kolar-Požun
- Mitja Jančič
- Miha Rot
- Gregor Kosec

#### Repository structure overview
- **Code** C++ and Python codes used to generate the data and the figures.
- **FiguresPaper** All the figures used in the paper.
- **Paper** All the LaTeX files used to generate the paper.



For any questions regarding the paper we are available at andrej.pozun@ijs.si