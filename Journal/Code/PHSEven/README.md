Headers for Even Polyharmonic RBF (of form r^(2k) log(r)). 
These must be included in order to compile the PoissonSweepOtherDegrees.cpp file, as it is not a standard part of Medusa library