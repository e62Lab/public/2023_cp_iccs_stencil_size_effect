#ifndef MEDUSA_BITS_APPROXIMATIONS_POLYHARMONICEVEN_FWD_HPP_
#define MEDUSA_BITS_APPROXIMATIONS_POLYHARMONICEVEN_FWD_HPP_


#include <medusa/Config.hpp>
#include <medusa/bits/utils/numutils.hpp>
#include <cmath>
#include "Operators_fwd.hpp"

namespace mm {

/**
 * Polyharmonic Radial Basis Function of even order.
**/
template <typename scal_t, int k = -1>
class PolyharmonicEven {
  public:
    static_assert((k > 0 && k % 2 == 0 && k<=6) || k == -1, "k must be either even and not higher than 6 or -1 (Dynamic)");
    typedef scal_t scalar_t;  ///< Scalar type used for computations.

  private:
    const int order_;  ///< Exponent of the RBF.

  public:
    /// Default constructor. Only applicable when order given as template argument.
    PolyharmonicEven();
    /// If template argument was -1, the order is accepted as runtime parameter.
    PolyharmonicEven(int order);

    /// Get order of the RBF.
    int order() const { return order_; }

    /**
     * Evaluate derivative of this RBF wrt. `r2` at given point.
     *
     * @param r2 Squared radial distance.
     * @param derivative Order of derivative to evaluate.
     * @throw Assertion fails if derivative order is negative.
     */
    inline scalar_t operator()(scalar_t r2, int derivative) const;

    /**
     * Evaluate Laplacian of this RBF wrt. `r` at given point.
     * @param r2 Squared radial distance.
     * @param lap Laplacian operator object.
     */
    template <int dimension>
    inline scalar_t operator()(scalar_t r2, Lap<dimension> lap) const;

    /// Evaluate this RBF given squared radial distance.
    inline scalar_t operator()(scalar_t r2) const { 
      if (r2 < 1e-14) return 0;
      return 0.5*std::log(r2)*ipow(std::sqrt(r2),order_);
      }

    /// Output basic information about given Gaussian RBF.
    template <typename S, int K>
    friend std::ostream& operator<<(std::ostream& os, const PolyharmonicEven<S, K>& m);
};

}  // namespace mm

#endif
