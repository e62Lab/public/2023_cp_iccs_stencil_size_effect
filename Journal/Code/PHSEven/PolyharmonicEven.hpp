#ifndef MEDUSA_BITS_APPROXIMATIONS_POLYHARMONICEVEN_HPP_
#define MEDUSA_BITS_APPROXIMATIONS_POLYHARMONICEVEN_HPP_

/**
 * @file
 * Implementation of Polyharmonic RBF for Even k.
 */

#include "PolyharmonicEven_fwd.hpp"
#include <medusa/bits/utils/numutils.hpp>

namespace mm {

template <typename scal_t, int k>
PolyharmonicEven<scal_t, k>::PolyharmonicEven() : order_(k) {
    assert_msg(order_ > 0, "Order must be supplied if compile type order is -1.");
}

template <typename scal_t, int k>
PolyharmonicEven<scal_t, k>::PolyharmonicEven(int order) : order_((k < 0) ? order : k) {
    assert_msg(order % 2 == 0 && order <= 6, "Order must be even and not higher than 6, got %d.", order);
}

template <typename scal_t, int k>
scal_t PolyharmonicEven<scal_t, k>::operator()(scal_t r2, int derivative) const {
    assert_msg(derivative >= 0, "Derivative of negative order %d requested.", derivative);
    assert_msg(derivative <= 2, "Derivatives above 2 are not supported. Got %d.", derivative);
    assert_msg(false,"Haven't tested if the derivatives (only used the laplacian) are correct. Please check for yourself and then delete this assert");
    if (r2 < 1e-15) {
        return 0;
    }
    if (derivative == 0) return (*this)(r2);
    if (derivative == 1){ 
        if (order_ == 2) return 0.5*(std::log(r2) + 1.0);
        if (order_ == 4) return r2*(std::log(r2)+0.5);
        if (order_ == 6) return r2*r2*(1.5*std::log(r2)+0.5);
    }
    if (derivative == 2){
        assert_msg(order_ == 2, "2nd derivative for order 2 has a singularity");
        if (order_ == 4) return std::log(r2) + 1.5;
        if (order_ == 6) return r2*(3.0*std::log(r2)+2.5);
    }
}
/// @cond
template <typename scal_t, int k>
template <int dimension>
scal_t PolyharmonicEven<scal_t, k>::operator()(scal_t r2, Lap<dimension>) const {
    assert_msg(order_ > 2, "Order must be > 2 to compute the Laplacian, got %d.", k);
    if (order_ ==4) {
        if (r2 < 1e-14) return 0;
        return ipow(std::sqrt(r2),2)*(8.0*std::log(r2)+8.0);
    } else if (order_ == 6){
        if (r2 < 1e-14) return 0;
        return 12.0*ipow(std::sqrt(r2),4)*(3.0/2.0*std::log(r2)+1.0);
    }
}
/// @endcond

/// Output basic information about given Gaussian RBF.
template <class S, int K>
std::ostream& operator<<(std::ostream& os, const PolyharmonicEven<S, K>& b) {
    return os << "Polyharmonic RBF of order " << b.order_ << '.';
}

}  // namespace mm

#endif  // MEDUSA_BITS_APPROXIMATIONS_POLYHARMONIC_HPP_
