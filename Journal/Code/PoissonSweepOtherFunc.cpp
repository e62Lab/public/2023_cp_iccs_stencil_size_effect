#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>

using namespace mm;
using namespace Eigen;

//SOLVE THE POISSON PROBLEM FOR DIFFERENT U

//x^4 y^5
double monom(double x,double y) {
    return x*x*x*x*y*y*y*y*y;
}
double monomLap(double x, double y){
    return 4*x*x*y*y*y*(5*x*x+3*y*y);
}

//sin(x)
double sinx(double x, double y){
    return std::sin(PI*x);
}
double sinxLap(double x, double y){
    return -PI*PI*std::sin(PI*x);
}

//cos(pi x) cos(pi y)
double cosinuses(double x, double y){
    return std::cos(PI*x)*std::cos(PI*y);
}
double cosinusesLap(double x, double y){
    return -2*PI*PI*cosinuses(x,y);
}

double bayonas(double x, double y){
    return 1 + std::sin(4*x) + std::cos(3*x) + std::sin(2*y);
}
double bayonasLap(double x, double y){
    return -16*std::sin(4*x) - 9*std::cos(3*x) - 4*std::sin(2*y);
}

double arctan(double x, double y){
    return std::atan(x*x+y*y);
}
double arctanLap(double x, double y){
    double num = 4*(-1+x*x*x*x+2*x*x*y*y+y*y*y*y);
    double denom = (1+x*x*x*x+2*x*x*y*y+y*y*y*y);
    return -num/denom/denom;
}

double franke(double x, double y){
    double term1 = 0.75*exp(-pow(9*x-2,2)/4 - pow(9*y-2,2)/4);
    double term2 = 0.75*exp(-pow(9*x+1,2)/49 - pow(9*y+1,1)/10);
    double term3 = 0.5*exp(-pow(9*x-7,2)/4 - pow(9*y-3,2)/4);
    double term4 = -0.2*exp(-pow(9*x-4,2) - pow(9*y-7,2));
    return term1+term2+term3+term4;
}
double frankeLap(double x, double y){
    double term1 = exp(-1.0/49*pow(1+9*x,2)-9*y/10)*(-1.60236+1.64838*x+7.41771*x*x);
    double term2 = exp(-pow(4-9*x,2)-pow(7-9*y,2))*(-4147.2+4665.6*x-5248.8*x*x+8164.8*y-5248.8*y*y);
    double term3 = 166.488*exp(-9.0/4*(-4*x+9*x*x+y*(-4+9*y)))*(0.0493827-0.444444*x+x*x-0.444444*y+y*y);
    double term4 = exp(-1.0/4*pow(7-9*x,2)-9.0/4*pow(1-3*y,2))*(546.75-1275.75*x+820.125*x*x-546.75*y+820.125*y*y);
    return term1+term2+term3+term4;
}

double asinh(double x, double y){ //asinh(x+2y)
    return std::log(std::sqrt(pow(x+2*y,2)+1)+x+2*y);
}
double asinhLap(double x, double y){
    return -5*(x+2*y)/pow(1+pow(x+2*y,2),1.5);  
}

double expo(double x, double y){ //exp(x^2+y^2)
    return std::exp(x*x+y*y);
}
double expoLap(double x, double y){
    return 4*expo(x,y)*(1+x*x+y*y);
}

double expoX(double x, double y){
    return std::exp(x*x);
}
double expoXLap(double x, double y){
    return std::exp(x*x)*(2.0+4.0*x*x);
}


void solve(std::string name, double (*solFunc)(double,double), double (*solLap)(double,double)) {
    int m = 3;
    double dx = 0.01;
    std::ostringstream strs;

    BallShape<Vec2d> ball({0.5,0.5}, 0.5);

    DomainDiscretization<Vec2d> domain = ball.discretizeBoundaryWithStep(dx);
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(1);
    domain.fill(fill_engine, dx);

    int N = domain.size();
    VectorXd rhs(N); rhs.setZero();

    strs.str("");
    strs << "Data/PoissonResultsFunc" << name << ".h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);
    hdf_out.writeDouble2DArray("positions",domain.positions());

    int startN = 10;
    int toCheck = 60;

    for (int n=startN;n<startN+toCheck;++n){
        FindClosest findSupportEdge = FindClosest(n);
        domain.findSupport(findSupportEdge);
        Monomials<Vec2d> mon(m);
        RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({}, mon);
        auto storage = domain.computeShapes(approx);
        SparseMatrix<double, Eigen::RowMajor> M(N, N);
        M.reserve(storage.supportSizes());
        rhs.setZero();
        auto op = storage.implicitOperators(M, rhs);

        for (int i : domain.interior()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.lap(i) = (*solLap)(x,y);

        }
        for (int i : domain.boundary()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.value(i) = (*solFunc)(x,y);
        }
        
        Eigen::SparseLU<decltype(M), COLAMDOrdering<int>> solver;
        solver.compute(M);
        ScalarFieldd u = solver.solve(rhs);

        strs.str("");
        strs << "SolutionN" << n;
        hdf_out.writeDoubleArray(strs.str(),u);
    }
    hdf_out.close();
}

int main(){

    std::vector<std::string> names {"x4y5","sinx", "cosxcosy", "bayona","atan", "franke", "asinh","exp","expX"}; 
    std::vector<double(*)(double,double)> funcs {&monom,&sinx, &cosinuses, &bayonas,&arctan,&franke, &asinh, &expo, &expoX};
    std::vector<double(*)(double,double)> laps {&monomLap,&sinxLap, &cosinusesLap, &bayonasLap,&arctanLap,&frankeLap, &asinhLap, &expoLap, &expoXLap};
    for (int i=0;i<9;++i){
        solve(names[i],funcs[i],laps[i]);
    }
    return 0;
}
