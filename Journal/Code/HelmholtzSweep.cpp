#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>

using namespace mm;
using namespace Eigen;

//SOLVE THE HELMHOLTZ EQUATION LAP(U) + K^2 U = F

void solve(int seed,double coeff) {
    int m = 3;
    double dx = 0.01;
    std::ostringstream strs;

    //BoxShape<Vec2d> ball({0.0,0.0},{1.0,1.0});
    BallShape<Vec2d> ball({0.5,0.5}, 0.5);

    DomainDiscretization<Vec2d> domain = ball.discretizeBoundaryWithStep(dx);
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(seed);
    domain.fill(fill_engine, dx);

    int N = domain.size();
    VectorXd rhs(N); rhs.setZero();

    strs.str("");
    int coeffFlag = static_cast<int>(coeff);
    if (coeffFlag == 1){
        strs << "Data/HelmholtzPoissonResultsSeed" << seed << ".h5";
    } else if (coeffFlag == 10){
        strs << "Data/Helmholtz2PoissonResultsSeed" << seed << ".h5";

    }
    HDF hdf_out(strs.str(), HDF::DESTROY);
    hdf_out.writeDouble2DArray("positions",domain.positions());

    int startN = 10; //starting stencil size
    int toCheck = 60; //number of stencil sizes to check

    for (int n=startN;n<startN+toCheck;++n){
        FindClosest findSupportEdge = FindClosest(n);
        domain.findSupport(findSupportEdge);
        Monomials<Vec2d> mon(m);
        RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({}, mon);
        auto storage = domain.computeShapes(approx);
        SparseMatrix<double, Eigen::RowMajor> M(N, N);
        M.reserve(storage.supportSizes());
        rhs.setZero();
        auto op = storage.implicitOperators(M, rhs);

        for (int i : domain.interior()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.lap(i) + coeff*op.value(i) = (-2*PI*PI+coeff)*std::sin(PI*x)*std::sin(PI*y);
            //op.lap(i) = -16.0*std::sin(4*x) - 9.0*std::cos(3*x) - 4.0*std::sin(2*y);
        }
        for (int i : domain.boundary()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.value(i) = std::sin(PI*x)*std::sin(PI*y);
            //op.value(i) = 1.0 + std::sin(4*x) + std::cos(3*x) + std::sin(2*y);
        }
        
        //Eigen::SparseLU<decltype(M), COLAMDOrdering<int>> solver;
        Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;
        solver.preconditioner().setDroptol(1e-5);
        solver.preconditioner().setFillfactor(50);
        solver.setMaxIterations(200);
        solver.setTolerance(1e-14);
        solver.compute(M);
        ScalarFieldd u = solver.solve(rhs);

        strs.str("");
        strs << "SolutionN" << n;
        hdf_out.writeDoubleArray(strs.str(),u);

        strs.str("");
        strs << "SupportsN" << n;
        hdf_out.writeDouble2DArray(strs.str(),domain.supports());
    }
    hdf_out.close();
}

int main(){
    solve(1,1);
    solve(1,10);
    return 0;
}
