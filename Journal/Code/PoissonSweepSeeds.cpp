#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>

using namespace mm;
using namespace Eigen;

//Solve the problem for several different seeds

void solve(int seed) {
    int m = 3;
    double dx = 0.01;
    std::ostringstream strs;

    BallShape<Vec2d> ball({0.5,0.5}, 0.5);

    DomainDiscretization<Vec2d> domain = ball.discretizeBoundaryWithStep(dx);
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(seed);
    domain.fill(fill_engine, dx);

    int N = domain.size();
    VectorXd rhs(N); rhs.setZero();

    strs.str("");
    strs << "Data/PoissonResultsSeed" << seed << ".h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);
    hdf_out.writeDouble2DArray("positions",domain.positions());

    int startN = 10; //starting stencil size
    int toCheck = 60; //number of stencil sizes to check

    for (int n=startN;n<startN+toCheck;++n){
        FindClosest findSupportEdge = FindClosest(n);
        domain.findSupport(findSupportEdge);
        Monomials<Vec2d> mon(m);;
        RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({}, mon);
        auto storage = domain.computeShapes(approx);
        SparseMatrix<double, Eigen::RowMajor> M(N, N);
        M.reserve(storage.supportSizes());
        rhs.setZero();
        auto op = storage.implicitOperators(M, rhs);

        for (int i : domain.interior()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.lap(i) = -2*PI*PI*(std::sin(PI*x)*std::sin(PI*y));
            //op.lap(i) = 5*4*x*x*x;
            //op.lap(i) = 16*std::exp(x*x+y*y)*(x*x*x*x+y*y*y*y+4*(x*x+y*y)+2*x*x*y*y+2);
            //op.lap(i) = 4*PI*PI*PI*PI*(std::sin(PI*x)*std::sin(PI*y));
        }
        for (int i : domain.boundary()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.value(i) = std::sin(PI*x)*std::sin(PI*y);
            //op.value(i) = x*x*x*x*x;
            //op.value(i) = 4*std::exp(x*x+y*y)*(1+x*x+y*y);
            //op.value(i) = -2*PI*PI*(std::sin(PI*x)*std::sin(PI*y));
        }
        
        Eigen::SparseLU<decltype(M), COLAMDOrdering<int>> solver;
        //Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;
        solver.compute(M);
        ScalarFieldd u = solver.solve(rhs);

        strs.str("");
        strs << "SolutionN" << n;
        hdf_out.writeDoubleArray(strs.str(),u);

        strs.str("");
        strs << "SupportsN" << n;
        hdf_out.writeDouble2DArray(strs.str(),domain.supports());
    }
    hdf_out.close();
}

int main(){
    for (int i=0;i<50;++i) solve(i);
    return 0;
}
