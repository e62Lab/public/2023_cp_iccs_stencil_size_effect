#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>
#include "PolyharmonicEven.hpp"

using namespace mm;
using namespace Eigen;

//SWEEP FOR DIFFERENT AUGMENTATION ORDER AND PHS DEGREES

void solve(int k, int m) {
    double dx = 0.01;
    std::ostringstream strs;

    //BoxShape<Vec2d> ball({0.0,0.0},{1.0,1.0});
    BallShape<Vec2d> ball({0.5,0.5}, 0.5);

    DomainDiscretization<Vec2d> domain = ball.discretizeBoundaryWithStep(dx);
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(1);
    domain.fill(fill_engine, dx);

    int N = domain.size();
    VectorXd rhs(N); rhs.setZero();

    strs.str("");
    strs << "Data/PoissonResultsk" << k << "M" << m << ".h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);
    hdf_out.writeDouble2DArray("positions",domain.positions());

    int startN = (m+1)*(m+2)/2; //starting stencil size
    int toCheck = 50; //number of stencil sizes to check

    for (int n=startN;n<startN+toCheck;++n){
        FindClosest findSupportEdge = FindClosest(n);
        domain.findSupport(findSupportEdge);
        Monomials<Vec2d> mon(m);
        RaggedShapeStorage<Vec2d,std::tuple<Lap<2>>> storage;
        if (k==3){
            RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({}, mon);
            storage = domain.computeShapes<sh::lap>(approx);
        } else if (k==4){
            RBFFD<PolyharmonicEven<double,4>,Vec2d,ScaleToFarthest> approx({}, mon);
            storage = domain.computeShapes<sh::lap>(approx);
        } else if (k==5){
            RBFFD<Polyharmonic<double,5>,Vec2d,ScaleToFarthest> approx({}, mon);
            storage = domain.computeShapes<sh::lap>(approx);      
        } else if (k==6){
            RBFFD<PolyharmonicEven<double,6>,Vec2d,ScaleToFarthest> approx({}, mon);
            storage = domain.computeShapes<sh::lap>(approx);
        }
        SparseMatrix<double, Eigen::RowMajor> M(N, N);
        M.reserve(storage.supportSizes());
        rhs.setZero();
        auto op = storage.implicitOperators(M, rhs);

        for (int i : domain.interior()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.lap(i) = -2*PI*PI*(std::sin(PI*x)*std::sin(PI*y));        }
        for (int i : domain.boundary()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.value(i) = std::sin(PI*x)*std::sin(PI*y);
        }
        
        //Eigen::SparseLU<decltype(M), COLAMDOrdering<int>> solver;
        Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;
        solver.preconditioner().setDroptol(1e-5);
        solver.preconditioner().setFillfactor(50);
        solver.setMaxIterations(200);
        solver.setTolerance(1e-14);
        solver.compute(M);
        ScalarFieldd u = solver.solve(rhs);

        strs.str("");
        strs << "SolutionN" << n;
        hdf_out.writeDoubleArray(strs.str(),u);

        strs.str("");
        strs << "SupportsN" << n;
        hdf_out.writeDouble2DArray(strs.str(),domain.supports());
    }
    hdf_out.close();
}

int main(){
    for (int k = 3;k<=6;++k){
        for (int m=2;m<=5;++m){
            if (k==6 && m==2) continue;
            solve(k,m);
        }
    }
    return 0;
}
