#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>
#include "Wendland.hpp"

using namespace mm;
using namespace Eigen;

//SCRIPT TO PREPAREP POISSON EQ. SOLUTION DATA FOR DIFFERENT RBFS

void solve(int rbfchoice, int shape) {
    int m = 3;
    double dx = 0.01;
    std::ostringstream strs;

    BallShape<Vec2d> ball({0.5,0.5}, 0.5);

    DomainDiscretization<Vec2d> domain = ball.discretizeBoundaryWithStep(dx);
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(1);
    domain.fill(fill_engine, dx);

    int N = domain.size();
    VectorXd rhs(N); rhs.setZero();

    strs.str("");
    strs << "Data/PoissonResultsRBF" << rbfchoice << "shape" << shape << ".h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);
    hdf_out.writeDouble2DArray("positions",domain.positions());

    int startN = 10;
    int toCheck = 60;

    for (int n=startN;n<startN+toCheck;++n){
        FindClosest findSupportEdge = FindClosest(n);
        domain.findSupport(findSupportEdge);
        Monomials<Vec2d> mon(m);
        RaggedShapeStorage<Vec2d,std::tuple<Lap<2>>> storage;
        double shapeVal = shape;
        if (rbfchoice == 0){
            Gaussian<double> g(shapeVal);
            RBFFD<Gaussian<double>,Vec2d,ScaleToFarthest> approx(g,mon);
            storage = domain.computeShapes<sh::lap>(approx);
        } else if (rbfchoice == 1){
            Multiquadric<double> m(shapeVal);
            RBFFD<Multiquadric<double>,Vec2d,ScaleToFarthest> approx(m,mon);
            storage = domain.computeShapes<sh::lap>(approx);
        } else if (rbfchoice == 2){
            InverseMultiquadric<double> im(shapeVal);
            RBFFD<InverseMultiquadric<double>,Vec2d,ScaleToFarthest> approx(im,mon);
            storage = domain.computeShapes<sh::lap>(approx);
        }
        SparseMatrix<double, Eigen::RowMajor> M(N, N);
        M.reserve(storage.supportSizes());
        rhs.setZero();
        auto op = storage.implicitOperators(M, rhs);

        for (int i : domain.interior()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.lap(i) = -2*PI*PI*(std::sin(PI*x)*std::sin(PI*y)); 

        }
        for (int i : domain.boundary()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.value(i) = std::sin(PI*x)*std::sin(PI*y);
        }
        
        Eigen::SparseLU<decltype(M), COLAMDOrdering<int>> solver;
        solver.compute(M);
        ScalarFieldd u = solver.solve(rhs);

        strs.str("");
        strs << "SolutionN" << n;
        hdf_out.writeDoubleArray(strs.str(),u);
    }
    hdf_out.close();
}

int main(){
    for (int i=0;i<=2;++i) {
        solve(i,1);
        solve(i,10);
    }
    return 0;
}
