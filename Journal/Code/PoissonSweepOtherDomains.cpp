#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>

using namespace mm;
using namespace Eigen;

//SCRIPT TO PREPARE POISSON EQUATION SOLUTION RESULTS ON DIFFERNET DOMAIN SHAPES

void solve(int domainNum) {
    int m = 3;
    double dx = 0.01;
    int seed = 1;
    std::ostringstream strs;


    /*Parametric domain, taken from our medusa wiki page*/
    auto example_r = [](Vec<double, 1> t) {
        double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
        return Vec2d(r/4 * cos(t(0)), r/4 * sin(t(0)));
    };
    auto example_r2 = [](Vec<double, 1> t) {
        double a = 1.0/8.0;
        double x = a*3.0*std::cos(t(0)) - a*std::cos(3.0*t(0));
        double y = a*3.0*std::sin(t(0)) - a*std::sin(3.0*t(0));
        return Vec2d(x,y);
    };

    auto der_example_r = [](Vec<double, 1> t) {
        double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
        double der_r = (-1.5 * pow(abs(cos(1.5 * t(0))),
                sin(3 * t(0))) * sin(3 * t(0)) * sin(1.5 * t(0)) +
                        3 * pow(abs(cos(1.5 * t(0))),
                                sin(3 * t(0))) * cos(3 * t(0)) * cos(1.5 * t(0))
                                * log(abs(cos(1.5 * t(0))))) / cos(1.5 * t(0));

        Eigen::Matrix<double, 2, 1> jm;
        jm.col(0) << der_r/4 * cos(t(0)) - r/4 * sin(t(0)), der_r/4 * sin(t(0)) + r/r * cos(t(0));

        return jm;
    };
    auto der_example_r2 = [](Vec<double, 1> t) {
        double a = 1.0/8.0;
        double derx = -a*3.0*std::sin(t(0)) + a*3.0*std::sin(3.0*t(0));
        double dery = a*3.0*std::cos(t(0)) - a*3.0*std::cos(3.0*t(0));
        derx=std::cos(t(0));
        dery=std::sin(t(0));
        Eigen::Matrix<double, 2, 1> jm;
        jm.col(0) << derx, dery;

        return jm;
    };

    BallShape<Vec2d> ball({0.5,0.5}, 0.5);
    DomainDiscretization<Vec2d> domain = ball.discretizeBoundaryWithStep(dx);
    switch (domainNum){
        case 20: //box
            {
            BoxShape<Vec2d> box({0.0,0.0},{1.0,1.0});
            domain = box.discretizeBoundaryWithStep(dx);
            }
            break;
        case 10: //rotatedBox
            {
            BoxShape<Vec2d> box({-0.5,-0.5},{0.5,0.5});
            domain = box.discretizeBoundaryWithStep(dx);
            domain = domain.rotate(PI/4);
            domain = domain.translate({0.5,0.5});
            }
            break;
        case 1: //triangle
            {
            PolygonShape<Vec2d> poly({{0.0,0.0},{1.0,0.0},{0.0,1.0}});
            domain = poly.discretizeBoundaryWithStep(dx);
            GeneralFill<Vec2d> fill_engine;
            fill_engine.seed(seed);
            domain.fill(fill_engine, dx);
            }
            break;
        case 2: //disc minus square
            {
            BallShape<Vec2d> ball({0.5,0.5},0.5);
            BoxShape<Vec2d> box({0.5,0.0},{1.0,0.5});
            domain = (ball-box).discretizeBoundaryWithStep(dx);
            domain = domain.translate({-0.5,-0.5});
            domain = domain.rotate(PI/4);
            domain = domain.translate({0.5,0.5});
            GeneralFill<Vec2d> fill_engine;
            fill_engine.seed(seed);
            domain.fill(fill_engine, dx);
            }
            break;
        case 3: //annulus
            {
            BallShape<Vec2d> ball({0.5,0.5},0.5);
            BallShape<Vec2d> ball2({0.5,0.5},0.1);
            domain = (ball-ball2).discretizeBoundaryWithStep(dx);
            GeneralFill<Vec2d> fill_engine;
            fill_engine.seed(seed);
            domain.fill(fill_engine, dx);
            }
            break;
        case 4: //parametric
            {
            BoxShape<Vec1d> param_bs(Vec<double,1>{0.0}, Vec<double,1>{2*PI});
            UnknownShape<Vec2d> shape;
            GeneralSurfaceFill<Vec2d, Vec1d> gsf;
            DomainDiscretization<Vec2d> domain2(shape);
            domain = domain2;
            domain.fill(gsf,param_bs,example_r,der_example_r,dx);
            domain = domain.translate({0.5,0.5});
            GeneralFill<Vec2d> fill_engine;
            fill_engine.seed(seed);
            domain.fill(fill_engine, dx);
            }
            break;
        case 0: //Peanut
            {
            BallShape<Vec2d> ball2({0.0,0.0},1.0);
            domain = ball2.discretizeBoundaryWithStep(dx);
            GeneralFill<Vec2d> fill_engine;
            fill_engine.seed(seed);
            domain.fill(fill_engine, dx);
            double scaling = 1.0/8.0;
            for (int i : domain.all()){
                double x = domain.pos(i,0);
                double y = domain.pos(i,1);
                double newX = x*(x*x-3*y*y+3);
                double newY = y*(3*x*x-y*y+3);
                domain.pos(i) = {newX*scaling,newY*scaling};
            }
            domain = domain.rotate(PI/4);
            domain = domain.translate({0.5,0.5});

            /*
            BoxShape<Vec1d> param_bs(Vec<double,1>{0.0}, Vec<double,1>{2*PI});
            UnknownShape<Vec2d> shape;
            GeneralSurfaceFill<Vec2d, Vec1d> gsf;
            DomainDiscretization<Vec2d> domain2(shape);
            domain = domain2;
            domain.fill(gsf,param_bs,example_r2,der_example_r2,dx);
            domain = domain.rotate(-PI/4);
            domain = domain.translate({0.5,0.5});
            */
            /*
            BallShape<Vec2d> ball1({0.3,0.5},0.3);
            BallShape<Vec2d> ball2({0.7,0.5},0.3);
            domain = (ball1+ball2).discretizeBoundaryWithStep(dx);
            domain = domain.translate({-0.5,-0.5});
            domain = domain.rotate(PI/4);
            domain = domain.translate({0.5,0.5});
            */
            }
            break;
    }


    int N = domain.size();
    VectorXd rhs(N); rhs.setZero();

    strs.str("");
    strs << "Data/PoissonResultsDomain" << domainNum << ".h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);
    hdf_out.writeDouble2DArray("positions",domain.positions());

    int startN = 10; //starting stencil size
    int toCheck = 60; //number of stencil sizes to check

    for (int n=startN;n<startN+toCheck;++n){
        FindClosest findSupportEdge = FindClosest(n);
        domain.findSupport(findSupportEdge);
        Monomials<Vec2d> mon(m);
        RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({}, mon);
        auto storage = domain.computeShapes(approx);
        SparseMatrix<double, Eigen::RowMajor> M(N, N);
        M.reserve(storage.supportSizes());
        rhs.setZero();
        auto op = storage.implicitOperators(M, rhs);

        for (int i : domain.interior()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.lap(i) = -2*PI*PI*(std::sin(PI*x)*std::sin(PI*y));
        }
        for (int i : domain.boundary()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.value(i) = std::sin(PI*x)*std::sin(PI*y);
        }
        
        Eigen::SparseLU<decltype(M), COLAMDOrdering<int>> solver;
        solver.compute(M);
        ScalarFieldd u = solver.solve(rhs);

        strs.str("");
        strs << "SolutionN" << n;
        hdf_out.writeDoubleArray(strs.str(),u);

        strs.str("");
        strs << "SupportsN" << n;
        hdf_out.writeDouble2DArray(strs.str(),domain.supports());
    }
    hdf_out.close();
}

int main(){
    for (int i=0;i<=4;++i) solve(i);
    return 0;
}
