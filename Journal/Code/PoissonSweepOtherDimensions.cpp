#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>

using namespace mm;
using namespace Eigen;

//SCRIPT TO PREPARE POISSON EQUATION SOLUTION FOR 1D 2D 3D with MIXED BC

void solve1D(bool dirichlet) {
    int m = 3;
    double dx = 0.01;
    int seed = 1;
    std::ostringstream strs;

    BallShape<Vec1d> ball({0.5}, 0.5);

    DomainDiscretization<Vec1d> domain = ball.discretizeBoundaryWithStep(dx);
    GeneralFill<Vec1d> fill_engine;
    fill_engine.seed(seed);
    domain.fill(fill_engine, dx);



    strs.str("");
    if (dirichlet){
        strs << "Data/PoissonResults1DSeed1.h5";
    } else {
        strs << "Data/PoissonResultsMixed1DSeed1.h5";
    }
    HDF hdf_out(strs.str(), HDF::DESTROY);
    Range<int> neu, dir;
    for (int i : domain.boundary()) {
        double x = domain.pos(i, 0);
        if (x > 0.5 || dirichlet) dir.push_back(i);
        else neu.push_back(i);
    }
    Range<int> gh = domain.addGhostNodes(dx, 0, neu);

    hdf_out.writeDouble2DArray("positions",domain.positions());
    int N = domain.size();
    VectorXd rhs(N); rhs.setZero();

    int startN = 4; //starting stencil size
    int toCheck = 30; //number of stencil sizes to check

    for (int n=startN;n<startN+toCheck;++n){
        FindClosest findSupportEdge = FindClosest(n);
        domain.findSupport(findSupportEdge);
        Monomials<Vec1d> mon(m);
        RBFFD<Polyharmonic<double,3>,Vec1d,ScaleToFarthest> approx({}, mon);
        auto storage = domain.computeShapes(approx);
        SparseMatrix<double, Eigen::RowMajor> M(N, N);
        M.reserve(storage.supportSizes());
        rhs.setZero();
        auto op = storage.implicitOperators(M, rhs);

        for (int i : domain.interior()) {
            double x = domain.pos(i,0);
            op.lap(i) = -PI*PI*(std::sin(PI*x));

        }
        for (int i : dir) {
            double x = domain.pos(i,0);
            op.value(i) = std::sin(PI*x);
        }
        for (int i : neu){
            double x = domain.pos(i,0);
            op.neumann(i, domain.normal(i)) = -PI*std::cos(PI*x);
            op.lap(i, gh[i]) = -PI*PI*std::sin(PI*x);
        }
        
        Eigen::SparseLU<decltype(M), COLAMDOrdering<int>> solver;
        //Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;
        solver.compute(M);
        ScalarFieldd u = solver.solve(rhs);

        strs.str("");
        strs << "SolutionN" << n;
        hdf_out.writeDoubleArray(strs.str(),u);

    }
    hdf_out.close();
}

void solve2D(bool dirichlet) {
    int m = 3;
    double dx = 0.01;
    int seed = 1;
    std::ostringstream strs;

    BallShape<Vec2d> ball({0.5,0.5}, 0.5);

    DomainDiscretization<Vec2d> domain = ball.discretizeBoundaryWithStep(dx);
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(seed);
    domain.fill(fill_engine, dx);

    strs.str("");
    if (dirichlet){
        strs << "Data/PoissonResults2DSeed1.h5";
    } else {
        strs << "Data/PoissonResultsMixed2DSeed1.h5";
    }    HDF hdf_out(strs.str(), HDF::DESTROY);
    Range<int> neu, dir;
    for (int i : domain.boundary()) {
        double x = domain.pos(i, 0);
        if (x > 0.5 || dirichlet) dir.push_back(i);
        else neu.push_back(i);
    }
    Range<int> gh = domain.addGhostNodes(dx, 0, neu);
    hdf_out.writeDouble2DArray("positions",domain.positions());
    int N = domain.size();
    VectorXd rhs(N); rhs.setZero();

    int startN = 10; //starting stencil size
    int toCheck = 190; //number of stencil sizes to check

    for (int n=startN;n<startN+toCheck;++n){
        FindClosest findSupportEdge = FindClosest(n);
        domain.findSupport(findSupportEdge);
        Monomials<Vec2d> mon(m);
        RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({}, mon);
        auto storage = domain.computeShapes(approx);
        SparseMatrix<double, Eigen::RowMajor> M(N, N);
        M.reserve(storage.supportSizes());
        rhs.setZero();
        auto op = storage.implicitOperators(M, rhs);

        for (int i : domain.interior()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.lap(i) = -2*PI*PI*(std::sin(PI*x)*std::sin(PI*y));

        }
        for (int i : dir) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.value(i) = std::sin(PI*x)*std::sin(PI*y);
        }
        for (int i : neu){
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.neumann(i, domain.normal(i)) = domain.normal(i).dot(Vec2d(PI*std::cos(PI*x)*std::sin(PI*y), PI*std::sin(PI*x)*std::cos(PI*y))); 
            op.lap(i, gh[i]) = -2*PI*PI*(std::sin(PI*x)*std::sin(PI*y));
        }
        
        Eigen::SparseLU<decltype(M), COLAMDOrdering<int>> solver;
        //Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;
        solver.compute(M);
        ScalarFieldd u = solver.solve(rhs);

        strs.str("");
        strs << "SolutionN" << n;
        hdf_out.writeDoubleArray(strs.str(),u);

    }
    hdf_out.close();
}

void solve3D(bool dirichlet) {
    int m = 3;
    double dx = 0.04;
    int seed = 1;
    std::ostringstream strs;

    BallShape<Vec3d> ball({0.5,0.5,0.5}, 0.5);

    DomainDiscretization<Vec3d> domain = ball.discretizeBoundaryWithStep(dx);
    GeneralFill<Vec3d> fill_engine;
    fill_engine.seed(seed);
    domain.fill(fill_engine, dx);

    strs.str("");
    if (dirichlet){
        strs << "Data/PoissonResults3DSeed1.h5";
    } else {
        strs << "Data/PoissonResultsMixed3DSeed1.h5";
    }    HDF hdf_out(strs.str(), HDF::DESTROY);
    Range<int> neu, dir;
    for (int i : domain.boundary()) {
        double x = domain.pos(i, 0);
        if (x > 0.5 || dirichlet) dir.push_back(i);
        else neu.push_back(i);
    }
    Range<int> gh = domain.addGhostNodes(dx, 0, neu);
    hdf_out.writeDouble2DArray("positions",domain.positions());
    int N = domain.size();
    VectorXd rhs(N); rhs.setZero();

    int startN = 20; //starting stencil size
    int toCheck = 180; //number of stencil sizes to check

    for (int n=startN;n<startN+toCheck;++n){
        FindClosest findSupportEdge = FindClosest(n);
        domain.findSupport(findSupportEdge);
        Monomials<Vec3d> mon(m);
        RBFFD<Polyharmonic<double,3>,Vec3d,ScaleToFarthest> approx({}, mon);
        auto storage = domain.computeShapes(approx);
        SparseMatrix<double, Eigen::RowMajor> M(N, N);
        M.reserve(storage.supportSizes());
        rhs.setZero();
        auto op = storage.implicitOperators(M, rhs);

        for (int i : domain.interior()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            double z = domain.pos(i,2);
            op.lap(i) = -3*PI*PI*(std::sin(PI*x)*std::sin(PI*y)*std::sin(PI*z));

        }
        for (int i : dir) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            double z = domain.pos(i,2);
            op.value(i) = std::sin(PI*x)*std::sin(PI*y)*std::sin(PI*z);
        }
        for (int i : neu){
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            double z = domain.pos(i,2);
            op.neumann(i, domain.normal(i)) = domain.normal(i).dot(Vec3d(PI*std::cos(PI*x)*std::sin(PI*y)*std::sin(PI*z), PI*std::sin(PI*x)*std::cos(PI*y)*std::sin(PI*z),PI*std::sin(PI*x)*std::sin(PI*y)*std::cos(PI*z))); 
            op.lap(i, gh[i]) = -3*PI*PI*(std::sin(PI*x)*std::sin(PI*y)*std::sin(PI*z));
        }
        
        Eigen::SparseLU<decltype(M), COLAMDOrdering<int>> solver;
        //Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;
        solver.compute(M);
        ScalarFieldd u = solver.solve(rhs);

        strs.str("");
        strs << "SolutionN" << n;
        hdf_out.writeDoubleArray(strs.str(),u);

    }
    hdf_out.close();
}

int main(){
    solve1D(true);
    solve2D(true);
    solve3D(true);
    solve1D(false);
    solve2D(false);
    solve3D(false);
    return 0;
}
