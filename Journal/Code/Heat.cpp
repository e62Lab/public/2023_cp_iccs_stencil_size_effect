#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>
#include<medusa/bits/domains/PolyhedronShape.hpp>

using namespace mm;
using namespace Eigen;

void solve(int n) {
    int m = 2;
    double dx = 0.035;
    int seed = 1;
    std::ostringstream strs;

    PolyhedronShape<Vec3d> heatsink = PolyhedronShape<Vec3d>::fromOFF("Heatsink.off");
    DomainDiscretization<Vec3d> domain = heatsink.discretizeBoundaryWithStep(dx,0);
    GeneralFill<Vec3d> fill; fill.seed(seed);
    fill(domain,dx);

    strs.str("");
    strs << "HeatSteady" << n << ".h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);

    int N0 = domain.size();
    Range<int> neu,dir,interior;
    for (int i=0;i<N0;++i){
        if (domain.type(i) > 0) interior.push_back(i);
        else {
            double y = domain.pos(i,1);
            if (y < 2.5583){
                dir.push_back(i);
            } else {
                neu.push_back(i);
            }
        }
    }
    hdf_out.writeDouble2DArray("positions",domain.positions());
    Range<int> gh = domain.addGhostNodes(0.025, 0, neu);
    //Range<int> gh = domain.addGhostNodes(dx, 0, neu);

    int N = domain.size();


    VectorXd rhs(N); rhs.setZero();
    Range<double> imex;


    FindClosest findSupportEdge = FindClosest(n);
    domain.findSupport(findSupportEdge);
    Monomials<Vec3d> mon(m);
    RBFFD<Polyharmonic<double,3>,Vec3d,ScaleToFarthest> approx({}, mon);
    auto storage = domain.computeShapes<sh::lap|sh::d1>(approx);
    
    SparseMatrix<double, Eigen::RowMajor> M(N, N);
    Range<int> per_row_v(N, n);
    M.reserve(per_row_v);
    auto op = storage.implicitOperators(M, rhs);

    for (int i : interior) {
        op.lap(i) = 0.0;
    }
    for (int i : dir) { 
        op.value(i) = 80.0;
    }
    for (int i : neu) { 
        op.value(i) + 209.0*op.neumann(i,domain.normal(i)) = 20.0;
    } 
    for (int i : neu){
        op.lap(i,gh[i]) = 0.0;
    }
    Eigen::SparseLU<decltype(M), COLAMDOrdering<int>> solver;
    //Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;
    solver.compute(M);
    ScalarFieldd u = solver.solve(rhs).head(N0);

    FindClosest findSupportIMEX = FindClosest(n).forNodes(domain.types() != 0).searchAmong(domain.types() != 0);
    domain.findSupport(findSupportIMEX);
    RBFFD<Polyharmonic<double,3>,Vec3d,ScaleToFarthest> approxIMEX({}, Monomials<Vec3d>(m+2));
    auto storageIMEX = domain.computeShapes<sh::lap>(approxIMEX);
    auto opIMEX = storageIMEX.explicitOperators(); 
    for (int i : interior){
        imex.push_back(opIMEX.lap(u,i));
    }
    u = u.head(N0);
    hdf_out.writeDoubleArray("Solution",u);
    hdf_out.writeDoubleArray("IMEX",imex);
    hdf_out.close();
}

int main(int argc, char *argv[]){
    int n = std::stoi(argv[1]);
    solve(n);
}
