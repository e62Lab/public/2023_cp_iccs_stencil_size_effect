#!/bin/bash

# make the Data folder if it doesn't exist yet.
if [ ! -d "./Data" ]
then
echo Making the Data directory...
mkdir Data
fi

echo Running all the scripts...
for filename in *.cpp #modify appropriately if only interested in running a single file
do
echo Compiling $filename...
filenameBase=$(basename $filename .cpp)
g++ -o $filenameBase "${filenameBase}.cpp" -O3 -I ../medusa/include -I /usr/include/hdf5/serial -L ../medusa/bin/ -L /usr/lib/x86_64-linux-gnu/hdf5/serial/ -l medusa_standalone -l hdf5 -Wall #Modify appropriately
echo Running it...
./$filenameBase
rm $filenameBase
done

echo Running the Python script...
python3 generatePlots.py