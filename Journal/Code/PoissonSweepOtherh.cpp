#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>

using namespace mm;
using namespace Eigen;

//SCRIPT TO PREPAREP POISSON EQ. SOLUTION DATA FOR DIFFERENT DISCRETIZATIONS

void solve(int factor) {
    int m = 3;
    double dx = 0.001*factor;
    std::ostringstream strs;

    BallShape<Vec2d> ball({0.5,0.5}, 0.5);

    DomainDiscretization<Vec2d> domain = ball.discretizeBoundaryWithStep(dx);
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(1);
    domain.fill(fill_engine, dx);

    int N = domain.size();
    VectorXd rhs(N); rhs.setZero();

    strs.str("");
    strs << "Data/PoissonResultshFactor" << factor << ".h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);
    hdf_out.writeDouble2DArray("positions",domain.positions());

    int startN = 10;
    int toCheck = 60;

    for (int n=startN;n<startN+toCheck;++n){
        FindClosest findSupportEdge = FindClosest(n);
        domain.findSupport(findSupportEdge);
        Monomials<Vec2d> mon(m);;
        RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({}, mon);
        auto storage = domain.computeShapes(approx);
        SparseMatrix<double, Eigen::RowMajor> M(N, N);
        M.reserve(storage.supportSizes());
        rhs.setZero();
        auto op = storage.implicitOperators(M, rhs);

        for (int i : domain.interior()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.lap(i) = -2*PI*PI*(std::sin(PI*x)*std::sin(PI*y)); 

        }
        for (int i : domain.boundary()) {
            double x = domain.pos(i,0);
            double y = domain.pos(i,1);
            op.value(i) = std::sin(PI*x)*std::sin(PI*y);
        }
        
        Eigen::SparseLU<decltype(M), COLAMDOrdering<int>> solver;
        solver.compute(M);
        ScalarFieldd u = solver.solve(rhs);

        strs.str("");
        strs << "SolutionN" << n;
        hdf_out.writeDoubleArray(strs.str(),u);
    }
    hdf_out.close();
}

int main(){
    solve(1);
    solve(5);
    solve(10);
    solve(20);
    solve(30);
    solve(50);
    return 0;
}
