#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>

using namespace mm;
using namespace Eigen;

//THIS SCRIPT PREPARES LAPLACE OPERATOR APPROXIMATION DATA

double f1(double x, double y,double a=1.0){
    return sin(a*PI*x)*sin(a*PI*y);
}


void solve(){
    int m = 3;
    double dx = 0.01;
    int seed = 1;
    std::ostringstream strs;

    BallShape<Vec2d> ball({0.5,0.5}, 0.5);

    DomainDiscretization<Vec2d> domain = ball.discretizeBoundaryWithStep(dx);
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(seed);
    domain.fill(fill_engine, dx);

    int N = domain.size();
    VectorXd rhs(N); rhs.setZero();

    HDF hdf_out("Data/LaplaceOpResults.h5", HDF::DESTROY);
    hdf_out.writeDouble2DArray("positions",domain.positions());

    int startN = 10; //starting stencil size
    int toCheck = 60; //number of stencil sizes to check
    VectorXi sizes(toCheck); sizes.setZero();

    for (int n=startN;n<startN+toCheck;++n){
        FindClosest find_support(n);
        domain.findSupport(find_support);
        VectorXd f = VectorXd(N); f.setZero();
        VectorXd ders = VectorXd(N); ders.setZero();
        for (int p : domain.all()){
            double x = domain.pos(p,0);
            double y = domain.pos(p,1);
            f(p) = f1(x,y);
        }
        Monomials<Vec2d> mon(m);
        RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({},mon);
        auto storage = domain.computeShapes(approx);
        auto op = storage.explicitOperators();
        for (int p : domain.all()){
            ders(p) = op.lap(f,p);
        }
        sizes[n-startN] = n;
        strs.str("");
        strs << "derXSize" << n;
        hdf_out.writeDoubleArray(strs.str(),ders);

        
        strs.str("");
        strs << "SupportsN" << n;
        hdf_out.writeDouble2DArray(strs.str(),domain.supports());
    }

    hdf_out.writeIntArray("support", sizes);
    hdf_out.closeFile();
}

int main() {
    solve();
    return 0;
}